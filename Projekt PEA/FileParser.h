﻿#pragma once
#define _USE_MATH_DEFINES
#include "TSP.h"
#include <string>
#include <fstream>

using namespace std;

class FileParser
{
	int numberOfFiles = 0;
	int parsedFiles = 0;

	vector<int> parseLowerDiag2FullMatrix(int dimension, vector<int>Edges);
	vector<int> parseUpperRow2FullMatrix(int dimension, vector<int>Edges);
	vector<int> parseEuc2D2FullMatrix(int dimension, vector<float>EdgesFl);
	vector<int> parseGeo2FullMatrix(int dimension, vector<float>EdgesFl);
	vector<int> normalize(int dimension, vector<int>Edges);
	
public:
	string openFile(string filePath);
	TSP loadFromFile(string path);
	vector<TSP> loadFromDirectory(string path);
	string progress() const { return parsedFiles + "/" + numberOfFiles; };

	void convertFile2FullMatrix(string filePath);
	void convertDirectory2FullMatrix(string path);

	
};
