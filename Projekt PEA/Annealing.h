#pragma once
#include "Algorithm.h"

using namespace std;
class Anneal : public Algorithm
{
	vector<int> getNextOrder(vector<int> order);

public:
	TSP anneal(TSP Tsp, double coolingRate = 0.90);

};