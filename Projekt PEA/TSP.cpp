#include "TSP.h"
#include <iostream>
#include <iomanip>
#include <sstream> 

TSP::TSP()
{
}

//TSP::TSP(int dimension)
//{
//	this->dimension = dimension;
//	adjacencyMatrix.assign(dimension, vector<int>(dimension));
//}

TSP::TSP(int dimension, vector<int> Edges)
{
	this->dimension = dimension;
	adjacencyMatrix.assign(dimension, vector<int>(dimension));

	for (int i = 0; i < dimension; i++)
	{
		for (int j = 0; j < dimension; j++)
		{
			adjacencyMatrix[i][j] = Edges[i * dimension + j];
		}
	}
}

TSP::TSP(string name, string type, int optimal, int dimension, vector<int> Edges)
{
	this->name = name;
	this->type = type;
	this->optimal = optimal;
	this->dimension = dimension;
	adjacencyMatrix.assign(dimension, vector<int>(dimension));

	for (int i = 0; i < dimension; i++)
	{
		for (int j = 0; j < dimension; j++)
		{
			adjacencyMatrix[i][j] = Edges[i * dimension + j];
		}
	}
}

TSP::~TSP()
{
}

void TSP::updateSolution(vector<int> solution)
{
	this->solution = solution;

	totalDistance = 0;

	for (int i = 0; i < solution.size() - 1; i++)
	{
		totalDistance += adjacencyMatrix[solution[i]][solution[i + 1]];
	}

	if (solution.size() > 0) totalDistance += adjacencyMatrix[solution[solution.size() - 1]][solution[0]];
}

string TSP::getPath()
{
	string path = "";
	for (int i = 0; i < dimension; i++) path += to_string(solution[i]) + " -> ";
	path += "0";
	return path;
}

string to_string(TSP tsp)
{
	stringstream string;

	if (tsp.getName() != "TSP") string << "Name: " << tsp.getName() << endl;

	int dimension = tsp.getDimension();
	for (int i = 0; i < dimension; i++)
	{
		for (int j = 0; j < dimension; j++)
		{
			string.width(4);
			string << tsp[i][j];
		}
		string << endl;
	}

	return string.str();
}
