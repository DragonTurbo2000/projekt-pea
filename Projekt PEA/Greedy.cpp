﻿#include "Greedy.h"

TSP Greedy::greedy(TSP tsp)
{
	vector <bool> Visited(tsp.getDimension(), false);

	currentOrder.resize(tsp.getDimension() + 1);

	int Ind_X, Ind_Y;

	int min = INT_MAX;

	for (int i = 0; i < tsp.getDimension(); i++)
	{
		for (int j = 0; j < tsp.getDimension(); j++)
		{
			if (tsp[i][j] < min && tsp[i][j] > 0)
			{
				min = tsp[i][j], Ind_X = i, Ind_Y = j;
			}
		}
	}

	currentOrder[0] = Ind_X;
	Visited[Ind_X] = true;

	for (int i = 1; i < tsp.getDimension(); i++)
	{
		min = INT_MAX;

		for (int j = 0; j < tsp.getDimension(); j++)
		{
			if (!Visited[j] && tsp[Ind_X][j] < min && tsp[Ind_X][j] > 0)
			{
				min = tsp[Ind_X][j], Ind_Y = j;
			}
		}

		Visited[Ind_Y] = true;
		Ind_X = Ind_Y;
		currentOrder[i] = Ind_X;
	}

	currentOrder[tsp.getDimension()] = currentOrder[0];

	tsp.updateSolution(currentOrder);

	return tsp;
}
