﻿#pragma once
#include "TSP.h"
#include "Algorithm.h"

class Greedy : public Algorithm
{
public:
	TSP greedy(TSP tsp);

};
