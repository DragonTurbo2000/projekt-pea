﻿#include "Testing.h"
#include "FileParser.h"
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include"gnuplot_i.hpp"
#include "Tabu.h"
#include "Greedy.h"
#include "AntAlgorithm.h"
#include "Genetic.h"

void Testing::testing(string path, string filename)
{

	ofstream file;
	file.open(filename, ios::out);

	FileParser parser;
	
	vector<TSP> tsp = parser.loadFromDirectory(path);
	TSP solution;
	
	
	Anneal anneal;
	Tabu tabu;
	Greedy greedy;
	AntAlgorithm aco;
	Genetic genetic;
	
	int result = INT_MAX;

	int repeats = 5;

	vector<int> X_values(repeats);
	vector<int> Y_values(repeats);
	for (int i = 0; i < repeats; i++) X_values[i] = i + 1;
	
	vector<double> coolingRates{ 0.5, 0.6, 0.75, 0.90, 0.99 };
	vector<int> tabuSize{ 10, 30, 50 };
	vector<int> iterations{ 10, 20, 30 };
	
	chrono::time_point<chrono::system_clock> start, end;
	
	cout << setw(13) << left << "| Zestaw" << setw(13) << left << "| Wynik" << setw(13) << left << "| Rozw.opt" << setw(13) << left << "| Śr. czas" << endl;
	file << "Zestaw\tNr. Próby\tWynik\tRozw. opt.\tWsp. Styg.\tŚr. czas\n";

	for (int i = 0; i < tsp.size(); i++)
	{
		Gnuplot gnuplot;
		gnuplot.set_style("linespoints");

		cout << "| " << setw(11) << left << tsp[i].getName() << "| ";
		gnuplot.set_title(tsp[i].getName());
		gnuplot << "set autoscale y\n";
		
		chrono::duration<double> elapsedTime(0);
		chrono::duration<double> avgTime(0);
		result = INT_MAX;

		/*for (int c = 0; c < coolingRates.size(); c++)
		{*/
		for (int c = 0; c < iterations.size(); c++)
		{
			for (int j = 1; j <= repeats; j++)
			{
				start = chrono::system_clock::now();
				//solution = anneal.anneal(tsp[i], coolingRates[c]);
				//solution = aco.startAlgorithm(tsp[i], 10, iterations[c]);
				//solution = greedy.greedy(tsp[i]);
				solution = genetic.GeneticAlgorithm(tsp[i]);
				Y_values[j-1] = solution.getTotalDistance();
				end = chrono::system_clock::now();
				avgTime += end - start;
				elapsedTime = end - start;
				file << solution.getName() << "\t" << j << "\t" << genetic.bestSolution << "\t" << solution.getOptimal() << "\t" << elapsedTime.count() << endl;
				if (solution.getTotalDistance() < result) result = solution.getTotalDistance();
			}
			//gnuplot.plot_xy(X_values, Y_values, to_string(coolingRates[c]));
			gnuplot.plot_xy(X_values, Y_values, to_string(tabuSize[c]));
		}

		Y_values.assign(repeats, solution.getOptimal());
		gnuplot.set_style("lines");
		gnuplot.plot_xy(X_values, Y_values, "rozw. opt.");

		gnuplot << "set terminal svg size 640,452\n";
		gnuplot << "set output 'Wykresy/" + tsp[i].getName() + ".svg'\n";
		gnuplot.replot();
		_sleep(1000); // żeby gnuplot zdążył narysować wykres nim destruktor usunie pliki tymczasowe 
		avgTime /= repeats;
		cout << setw(11) << left << result << "| " << setw(11) << left << solution.getOptimal() << "| " << avgTime.count() << endl;
		
	}
	file.close();
}
