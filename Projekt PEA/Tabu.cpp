#include "Tabu.h"
#include <list>
#include "Greedy.h"
#include <ctime>

TSP Tabu::tabu(TSP tsp, int maxTabuSize)
{
	double totalTime = 0;
	this->tsp = tsp;
	dimension = tsp.getDimension();
	this->tabuList = new TabuList(maxTabuSize, dimension);
	int orderNotChanged = 0;
	
	Greedy greedy;
	currentOrder = greedy.greedy(tsp).getSolution();

	bestOrder = currentOrder;	

	int bestOrderDistance = getTotalDistance(bestOrder);

	clock_t begin = clock();
	while (totalTime < 3)
	{
		
		currentOrder = getBestNeighbour(currentOrder);
		int currentOrderDistance = getTotalDistance(currentOrder);

		if (currentOrderDistance < bestOrderDistance)
		{
			bestOrder = currentOrder;
			bestOrderDistance = getTotalDistance(bestOrder);
			orderNotChanged = 0;
		}
		else
		{
			orderNotChanged++;
			tabuList->insert(bestChange[0], bestChange[1]);
		}

		if (orderNotChanged == 100)
		{
			for (int i = 0; i < 100; i++) randomSwap(currentOrder);

			orderNotChanged = 0;
			tabuList->clear();
		}

		clock_t end = clock();
		double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
		totalTime = elapsed_secs;
	}

	tsp.updateSolution(bestOrder);
	return tsp;
}

vector<int> Tabu::getBestNeighbour(vector<int> order)
{
	vector<int> nextOrder(order);
	int repeats = 0;

	for (int i = 0; i < sqrt(dimension); i++)
	{
		vector<int> newOrder(nextOrder);

		int randomCity = rand() % (dimension - 1) + 1;		//generujemy dwie warto�ci r�ne od zera oraz od siebie
		int anotherRandomCity = rand() % (dimension - 1) + 1;
		while (randomCity == anotherRandomCity) anotherRandomCity = rand() % (dimension - 1) + 1;

		swap(newOrder[randomCity], newOrder[anotherRandomCity]);

		if (!tabuList->contains(randomCity, anotherRandomCity) 
			&& getTotalDistance(newOrder) < getTotalDistance(nextOrder))
		{
			nextOrder = newOrder;
			bestChange[0] = randomCity;
			bestChange[1] = anotherRandomCity;
		}

	}
	return nextOrder;
}

void Tabu::randomSwap(vector<int> &order)
{
	int randomCity = rand() % (dimension - 1) + 1;		//generujemy dwie warto�ci r�ne od zera oraz od siebie
	int anotherRandomCity = rand() % (dimension - 1) + 1;
	while (randomCity == anotherRandomCity) anotherRandomCity = rand() % (dimension - 1) + 1;

	swap(order[randomCity], order[anotherRandomCity]);
}

