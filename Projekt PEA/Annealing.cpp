#include "Annealing.h"
#include "Greedy.h"

TSP Anneal::anneal(TSP Tsp, double coolingRate)
{
	double temperature = pow((double)tsp.getDimension(), 2) * 1000.0;
	double absoluteTemperature = 0.00001;
	this->tsp = Tsp;
	dimension = tsp.getDimension();
	Greedy greedy;

	currentOrder = greedy.greedy(tsp).getSolution();
	//currentOrder.resize(dimension);
	//for (int i = 0; i < dimension; i++) currentOrder[i] = i;

	vector<int> bestOrder = currentOrder;

	int iteration = -1;
	
	double deltaDistance = 0;

	double distance = getTotalDistance(currentOrder);
	double bestDistance = distance;
	uniform_real_distribution<double> distribution(0.0, 1.0);
	default_random_engine generator;

	while (temperature > absoluteTemperature)
	{
		nextOrder = getNextOrder(currentOrder);

		deltaDistance = getTotalDistance(nextOrder) - distance;


		if ((deltaDistance < 0) || (distance > 0 && exp(-deltaDistance / temperature) > distribution(generator)))
		{
			currentOrder = nextOrder;

			distance = deltaDistance + distance;
		}

		if (distance < bestDistance) bestOrder = currentOrder;
		else if (distance > bestDistance * 1.5)
		{
			currentOrder = bestOrder;
			distance = bestDistance;
		}


		temperature *= coolingRate;

		iteration++;
	}

	tsp.updateSolution(bestOrder);
	return tsp;
}

vector<int> Anneal::getNextOrder(vector<int> order)
{
	vector<int> newOrder(order);

	int randomCity = rand() % (dimension - 1) + 1;		//generujemy dwie warto�ci r�ne od zera oraz od siebie
	int anotherRandomCity = rand() % (dimension - 1) + 1;
	while (randomCity == anotherRandomCity) anotherRandomCity = rand() % (dimension - 1) + 1;

	swap(newOrder[randomCity], newOrder[anotherRandomCity]);

	return newOrder;
}
