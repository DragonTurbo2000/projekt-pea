#pragma once

#include <list>
#include <vector>

using namespace std;

class TabuList
{
private:

	struct Node
	{
		int verA;
		int verB;
	};

	int tabuListMaxSize;
	int tabuListSize;

	list<Node> tabuList;
	int **tabuListMatrix;
	int dimension;


public:
	TabuList(int tabuListMaxSize, int dimension);
	~TabuList();

	bool contains(int vertexA, int vertexB);
	void insert(int vertexA, int vertexB);
	void clear();

};

