#include "Algorithm.h"



Algorithm::Algorithm()
{
}


Algorithm::~Algorithm()
{
}

int Algorithm::getTotalDistance(vector<int> order)
{
	int totalDistance = 0;

	for (int i = 0; i < order.size() - 1; i++)
	{
		totalDistance += tsp[order[i]][order[i + 1]];
	}

	if (order.size() > 0) totalDistance += tsp[order[order.size() - 1]][order[0]];

	return totalDistance;
}

