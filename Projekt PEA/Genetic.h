#pragma once
#include "Algorithm.h"

class Genetic : public Algorithm
{

private:
	vector<vector<int>> population;
	vector<vector<int>> parents;
	vector<vector<int>> children;

	int cityCount;

	int iterationNumber;
	int populationSize;
	int parentsPopulationSize;
	int childrenPairCount;
	double mutationProbability;

	int swapsInMutation;

	

	void generateRandomPermutation(vector<int>& cityPermutation, int cityCount);
	void swap(vector<int>& permutation, int idx1, int idx2);
	unsigned int calculateCMax(vector<int> cityPermutation);

	void initialize();
	void chooseBeginningPopulation();
	void chooseParents();
	void mutate(vector<int>& child);
	void tryMutate(vector<int>& permutation);
	void cross();
	bool permutationComparator(vector<int> i, vector<int> j);
	void chooseNewPopulation();
	unsigned int getBestSpecimen();
	void finalize();

public:
	Genetic();
	~Genetic();
	int bestSolution;
	vector<int> bestOrder;
	TSP GeneticAlgorithm(TSP tsp);
};

