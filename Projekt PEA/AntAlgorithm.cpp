#include "AntAlgorithm.h"
#include <math.h>
#include <random>


double fRand(double fMin, double fMax)
{
	double lower_bound = fMin;
	double upper_bound = fMax;
	std::uniform_real_distribution<double> unif(lower_bound, upper_bound);
	std::default_random_engine re;
	double a_random_double = unif(re);
	return a_random_double;
}

AntAlgorithm::AntAlgorithm()
{
	
}

AntAlgorithm::~AntAlgorithm()
{
}

void AntAlgorithm::initializePhero()
{
	for (int i = 0; i < Phero.size(); i++)
	{
		for (int j = 0; j < Phero[i].size(); j++)
		{
			Phero[i][j] = 0.5;
		}
	}
}

void AntAlgorithm::fadePhero()
{
	for (int i = 0; i < Phero.size(); i++)
	{
		for (int j = 0; j < Phero[i].size(); j++)
		{
			Phero[i][j] *= (1 - evapFactor);
		}
	}
}

void AntAlgorithm::placePhero(vector<int>tour)
{
	int src = tour[0];
	int dst;
	for (int i = tour.size() - 1; i >= 0; i--)
	{
		dst = src;
		src = tour[i];
		Phero[src][dst] += (1.0 / tsp.citiesDistance(src, dst));
	}
}

void AntAlgorithm::initAnts()
{
	ants.resize(colonySize);
	for (int i = 0; i < ants.size(); i++)
	{
		Ant ant(dimension, rand() % dimension);
		ants[i] = ant;
	}
	
}

void AntAlgorithm::runAnt(int antIndex)
{
	fadePhero();

	int currentCity;

	while (!ants[antIndex].visitedAllCities(dimension))
	{
		currentCity = ants[antIndex].getCurrentCity();
		int city = drawCity(antIndex);

		ants[antIndex].move(city);
		ants[antIndex].updateDistance(tsp.citiesDistance(currentCity, city));
	}

	currentCity = ants[antIndex].getCurrentCity();
	int initialCity = ants[antIndex].getInitialCity();

	ants[antIndex].move(initialCity);
	ants[antIndex].updateDistance(tsp.citiesDistance(currentCity, initialCity));

	placePhero(ants[antIndex].getCurrentOrder());

	if (totalLength > ants[antIndex].getTotalDistance())
	{
		bestSolution = ants[antIndex].getCurrentOrder();
		totalLength = ants[antIndex].getTotalDistance();
	}

	ants[antIndex].clear();
}

void AntAlgorithm::runAllAnts()
{
	for (int i = 0; i < ants.size(); i++)
	{
		runAnt(i);
	}
}


int AntAlgorithm::drawCity(int antIndex)
{
	//Ca�kowicie losowy wyb�r �cie�ki
	if (fRand(0.001, 0.5) < pr)
	{
		int randomCity = rand() % (dimension);
		int index = -1;
		for (int i = 0; i < dimension; i++)
		{
			if (!ants[antIndex].cityVisited(i)) index++;
			if (index == randomCity) return index;
		}
	}

	calculateProb(antIndex);

	double boundary = fRand(0.4, 1.0);
	while (true)
	{
		int index = (rand() % dimension);
		double probs = prob[index];
		if (probs >= boundary) return prob[index];

		if (probs != 0.0)
		{
			prob[index] += 0.2;
			probs = prob[index];
		}
	}

}

void AntAlgorithm::calculateProb(int antIndex)
{
	int currentCity = ants[antIndex].getCurrentCity();

	double denom = 0.0;
	for (int i = 0; i < dimension; i++)
	{
		if (!ants[antIndex].cityVisited(i))
		{
			denom += pow(Phero[currentCity][i], alphaFactor);
			denom *= pow(1.0 / tsp.citiesDistance(currentCity, i), betaFactor);
		}
	}

	for (int i = 0; i < dimension; i++)
	{
		if (ants[antIndex].cityVisited(i)) prob[i] = 0.0;
		else
		{
			double numerator = pow(Phero[currentCity][i], alphaFactor) * pow(1.0 / tsp.citiesDistance(currentCity, i), betaFactor);
			prob[i] = numerator / denom;
		}
	}
}

TSP AntAlgorithm::startAlgorithm(TSP tsp, int colonySize, int numberOfIterations)
{
	this->tsp = tsp;
	dimension = tsp.getDimension();
	prob.resize(dimension);
	Phero.assign(dimension, vector<double>(dimension));
	totalLength = INT_MAX;


	this->colonySize = colonySize;

	initializePhero();
	initAnts();

	int count = 0;
	while (count < numberOfIterations)
	{
		runAllAnts();
		count++;
	}

	tsp.updateSolution(bestSolution);

	return tsp;

}

