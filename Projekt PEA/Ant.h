#pragma once
#include <vector>
using namespace std;

class Ant
{
private:
	vector<int> currentOrder;

	bool *visitedCities;

	int initialCity;
	int currentCity;
	int totalDistance;

public:

	Ant() { }
	Ant(int dimension, int initialCity);
	~Ant();

	vector<int> getCurrentOrder() { return currentOrder; }
	int getCurrentCity() { return currentCity; }
	int getInitialCity() const { return initialCity; }

	int getTotalDistance() { return totalDistance; }
	bool visitedAllCities(int dimension) { return currentOrder.size() == dimension + 1; }
	bool cityVisited(int city) { return visitedCities[city]; }

	void move(int city);
	void updateDistance(int distance) { this->totalDistance += distance; }
	void clear();

};

