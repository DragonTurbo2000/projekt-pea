#include "Genetic.h"
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <climits>
#include <algorithm>
#include <Windows.h>
#include <time.h>
#include <conio.h>
#include "Greedy.h"

Genetic::Genetic()
{
	
	iterationNumber = 300;
	populationSize = 100;
	parentsPopulationSize = 50;
	childrenPairCount = 10;
	mutationProbability = 0.05;
}


Genetic::~Genetic()
{
}

double getRandomDouble()
{
	return (double)rand() / (double)RAND_MAX;
}

void Genetic::swap(vector<int>& permutation, int idx1, int idx2)
{
	int temp = permutation[idx1];
	permutation[idx1] = permutation[idx2];
	permutation[idx2] = temp;
}

unsigned int Genetic::calculateCMax(vector<int> cityPermutation)
{
	unsigned int sum = 0;
	for (int i = 0; i < cityCount - 1; ++i)
	{

		sum += tsp.citiesDistance(cityPermutation[i], cityPermutation[i+1]);
	}
	sum += tsp.citiesDistance(cityPermutation[cityCount - 1], cityPermutation[0]);
	unsigned int ret = (UINT_MAX - sum);
	return ret;
}

void Genetic::initialize()
{
	//population = new vector<int*>();
	population.reserve(populationSize);
	//parents = new vector<int*>();
	parents.reserve(parentsPopulationSize);
	//children = new vector<int*>();
	children.reserve(childrenPairCount * 2);

	bestSolution = INT_MAX;
}

void Genetic::generateRandomPermutation(vector<int>& cityPermutation, int cityCount)
{
	for (int i = 0; i < cityCount; ++i)
	{
		cityPermutation[i] = i;
	}
	for (int i = 0; i < cityCount; i++)
	{
		int r = rand() % (cityCount - i);
		int item = cityPermutation[i];
		cityPermutation[i] = cityPermutation[i + r];
		cityPermutation[i + r] = item;
	}
}

void Genetic::chooseBeginningPopulation()
{
	for (int i = 0; i < populationSize; ++i)
	{
		tsp.getDimension();
		vector<int> permutation(cityCount);
		generateRandomPermutation(permutation, cityCount);
		population.push_back(permutation);
	}
}

void Genetic::chooseParents()
{
	parents.clear();

	unsigned long long permutationSum = 0;
	vector <int> permutationValues(populationSize);
	unsigned long long currentValue = 0;
	unsigned long long lastValue = 0;

	//Wyliczenie sumy wszytkich funkji przystosowania
	for (int i = 0; i < populationSize; i++)
	{
		permutationValues[i] = calculateCMax(population[i]);
		permutationSum += permutationValues[i];
	}

	unsigned long long r;

	//Odszukanie funkcji celu odpowiadajacej wylosowanej wartosci
	for (int i = 0; i < parentsPopulationSize; i++)
	{
		r = getRandomDouble() * permutationSum;
		currentValue = 0;
		lastValue = 0;

		for (int i = 0; i < populationSize; i++)
		{
			currentValue += permutationValues[i];

			if (lastValue <= r && r <= currentValue)
			{
				parents.push_back(population.at(i));
				break;
			}
			lastValue += permutationValues[i];
		}
	}
}

void Genetic::mutate(vector<int>& child)
{
	for (int i = 0; i < swapsInMutation; ++i)
	{
		swap(child, rand() % cityCount, rand() % cityCount);
	}
}

void Genetic::tryMutate(vector<int>& permutation)
{
	if (getRandomDouble() < mutationProbability) mutate(permutation);
}

void Genetic::cross()
{
	vector<int> parent1 = parents.at(rand() % parentsPopulationSize);
	vector<int> parent2 = parents.at(rand() % parentsPopulationSize);

	vector<int> child1(cityCount);
	vector<int> child2(cityCount);

	for (int i = 0; i < cityCount; i++)
	{
		child1[i] = parent1[i];
		child2[i] = parent2[i];
	}

	int crossPoint1 = rand() % cityCount;
	int crossPoint2 = rand() % cityCount;

	if (crossPoint1 > crossPoint2)
	{
		int temp = crossPoint2;
		crossPoint2 = crossPoint1;
		crossPoint1 = temp;
	}

	for (int i = crossPoint1; i <= crossPoint2; ++i)
	{
		int temp = child1[i];
		child1[i] = child2[i];
		child2[i] = temp;
	}

	for (int i = crossPoint1; i <= crossPoint2; ++i)
	{
		for (int j = 0; j < crossPoint1; j++)
		{
			if (child1[j] == child1[i]) child1[j] = child2[i];
			if (child2[j] == child2[i]) child2[j] = child1[i];
		}
	}

	tryMutate(child1);
	tryMutate(child2);

	children.push_back(child1);
	children.push_back(child2);
}

bool Genetic::permutationComparator(vector<int> i, vector<int> j)
{
	return calculateCMax(i) < calculateCMax(j);
}

void Genetic::chooseNewPopulation()
{
	vector<vector<int>> newPopulation;
	newPopulation.reserve(populationSize);

	sort(population.begin(), population.end(), [this](auto i, auto j)->bool {return calculateCMax(i) < calculateCMax(j); });
	sort(children.begin(), children.end(), [this](auto i, auto j)->bool {return calculateCMax(i) < calculateCMax(j); });

	for (int i = 0; i < populationSize; i++)
	{
		//Jesli nie ma juz osobnikow w population
		if (children.empty() && !population.empty())
		{
			newPopulation.push_back(population.back());
			population.pop_back();
			continue;
		}

		//Jesli nie ma juz dzieci
		if (!children.empty() && population.empty())
		{
			newPopulation.push_back(children.back());
			children.pop_back();
			continue;
		}

		//Wybor korzystniejszego
		if (population.back() > children.back())
		{
			newPopulation.push_back(population.back());
			population.pop_back();
		}
		else
		{
			newPopulation.push_back(children.back());
			children.pop_back();
		}
	}

	while (!population.empty())
	{
		population.pop_back();
	}
	while (!children.empty())
	{
		children.pop_back();
	}

	population = newPopulation;
}

unsigned int Genetic::getBestSpecimen()
{
	unsigned int ret = calculateCMax(population.front());
	unsigned int r2 = (UINT_MAX - ret);
	return r2;
}

void Genetic::finalize()
{
	while (!population.empty())
	{
		population.pop_back();
	}

	parents.clear();
	population.clear();

}

TSP Genetic::GeneticAlgorithm(TSP tsp)
{
	this->tsp = tsp;
	cityCount = tsp.getDimension();

	initialize();
	chooseBeginningPopulation();
	for (int i = 0; i < iterationNumber; ++i)
	{
		chooseParents();
		// cross robi po jednej parze dzieci
		for (int i = 0; i < childrenPairCount; i++)
		{
			// tu nie ma mutate() bo cross ju� robi mutate()
			cross();
		}
		chooseNewPopulation();

		int tmpbest = getBestSpecimen();
		if (tmpbest < bestSolution)
		{
			bestSolution = tmpbest;
			bestOrder = population.front();
		}
	}

	vector<int> Order(cityCount);

	//finalize();
	tsp.updateSolution(bestOrder);
	return tsp;
}
