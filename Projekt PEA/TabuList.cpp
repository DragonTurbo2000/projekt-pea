#include "TabuList.h"



TabuList::TabuList(int tabuListMaxSize, int dimension)
{
	this->tabuListMaxSize = tabuListMaxSize;
	this->dimension = dimension;

	tabuListSize = 0;

	tabuListMatrix = new int *[dimension];
	for (int i = 0; i < dimension; i++)
	{
		tabuListMatrix[i] = new int[dimension];
	}
}


TabuList::~TabuList()
{
}

bool TabuList::contains(int vertexA, int vertexB)
{
	return tabuListMatrix[vertexA][vertexB] == 1;
}

void TabuList::insert(int vertexA, int vertexB)
{
	Node cities;
	cities.verA = vertexA;
	cities.verB = vertexB;

	if (tabuListSize < tabuListMaxSize)
	{
		tabuList.push_back(cities);
	}
	else
	{
		Node cities2 = tabuList.front();
		tabuList.pop_front();

		tabuListMatrix[cities2.verA][cities2.verB] = 0;
		tabuListMatrix[cities2.verB][cities2.verA] = 0;

		tabuListMatrix[vertexA][vertexB] = 1;
		tabuListMatrix[vertexB][vertexA] = 1;
		tabuList.push_back(cities);

	}
}

void TabuList::clear()
{
	tabuList.clear();
	for (int i = 0; i < dimension; i++)
	{
		for (int j = 0; j < dimension; j++)
		{
			tabuListMatrix[i][j] = 0;
		}
	}
}
