#pragma once

#include "TSP.h"
#include <vector>

class Algorithm
{
protected:
	TSP tsp;
	int dimension;

	int getTotalDistance(vector<int>);
	
	vector<int> currentOrder;
	vector<int> nextOrder;

public:
	Algorithm();
	~Algorithm();
};

