﻿#include "FileParser.h"
#include <iostream>
#include <sstream> 


#define strtk_no_tr1_or_boost
#include "strtk.hpp"
#include "dirent.h"
#include <iomanip>


string FileParser::openFile(string filePath)
{
	string content;

	std::ifstream file(filePath, ios::in);

	if (file)
	{
		std::string contents;
		file.seekg(0, std::ios::end);
		contents.resize(file.tellg());
		file.seekg(0, std::ios::beg);
		file.read(&contents[0], contents.size());
		file.close();
		return(contents);
	}

	throw exception("File not found");
}

TSP FileParser::loadFromFile(string path)
{
	string contents;
	try
	{
		contents = openFile(path);
	}
	catch(exception e)
	{
		cerr << "Nie znaleziono pliku!\n";
		return TSP();
	}

	auto iter1 = contents.begin();
	auto iter2 = contents.begin();

	string toParse;

	string name, type, weightType, weightFormat, tmp;
	int dimension, optimal;
	

	while (*iter2 != '\n') ++iter2;
	toParse.assign(iter1, iter2);
	strtk::parse(toParse, ": \n\t", tmp, name);

	++iter2;
	iter1 = iter2;
	while (*iter2 != '\n') ++iter2;
	toParse.assign(iter1, iter2);
	strtk::parse(toParse, ": \n\t", tmp, type);

	++iter2;
	iter1 = iter2;
	while (*iter2 != '\n') ++iter2;
	toParse.assign(iter1, iter2);
	strtk::parse(toParse, ": \n\t", tmp, optimal);

	++iter2;
	iter1 = iter2;
	while (*iter2 != '\n') ++iter2;
	toParse.assign(iter1, iter2);
	strtk::parse(toParse, ": \n\t", tmp, dimension);

	++iter2;
	iter1 = iter2;
	while (*iter2 != '\n') ++iter2;
	toParse.assign(iter1, iter2);
	strtk::parse(toParse, ": \n\t", tmp, weightType);
	
	iter1 = iter2;
	while (*iter1 != '0' && *iter1 != '1' && *iter1 != '2' && *iter1 != '3' && *iter1 != '4' && *iter1 != '5' &&
		*iter1 != '6' && *iter1 != '7' && *iter1 != '8' && *iter1 != '9') ++iter1;

	iter2 = iter1;
	--iter1;
	while (*iter2 != 'D' && *iter2 != 'E') ++iter2;
	toParse.assign(iter1, iter2);

	vector <int> Edges;

	if (weightType == "EUC_2D")
	{
		vector <float> EdgesFL;
		strtk::parse(toParse, ": \n\t", tmp, EdgesFL);
		Edges = parseEuc2D2FullMatrix(dimension, EdgesFL);

		return TSP(name, type, optimal, dimension, Edges);
	}
	if (weightType == "GEO")
	{
		vector <float> EdgesFL;
		strtk::parse(toParse, ": \n\t", tmp, EdgesFL);
		Edges = parseGeo2FullMatrix(dimension, EdgesFL);

		return TSP(name, type, optimal, dimension, Edges);
	}
	if (weightType == "FULL_MATRIX")
	{
		strtk::parse(toParse, ": \n\t", tmp, Edges);
		Edges = normalize(dimension, Edges);

		return TSP(name, type, optimal, dimension, Edges);
	}
	if (weightFormat == "LOWER_DIAG_ROW")
	{
		strtk::parse(toParse, ": \n\t", tmp, Edges);
		Edges = parseLowerDiag2FullMatrix(dimension, Edges);
		Edges = normalize(dimension, Edges);

		return TSP(name, type, optimal, dimension, Edges);
	}
	if (weightFormat == "UPPER_ROW" || weightFormat == "UPPER_DIAG_ROW")
	{
		strtk::parse(toParse, ": \n\t", tmp, Edges);
		Edges = parseUpperRow2FullMatrix(dimension, Edges);
		Edges = normalize(dimension, Edges);

		return TSP(name, type, optimal, dimension, Edges);
	}

	return TSP();
}

vector<TSP> FileParser::loadFromDirectory(string path)
{
	DIR *dir;
	struct dirent *ent;

	dir = opendir(path.c_str());

	vector<TSP> tsp;

	if (dir != NULL) 
	{
		while ((ent = readdir(dir)) != NULL) 
		{
			if(ent->d_type == DT_REG)
			{
				numberOfFiles++;
			}
		}

		rewinddir(dir);

		while ((ent = readdir(dir)) != NULL)
		{
			if (ent->d_type == DT_REG)
			{
				cout << setw(3) << parsedFiles << "/" << setw(4) << left << numberOfFiles << setw(13) << right << ent->d_name << '\r' << flush;
				string filePath = path + "/" + ent->d_name;
				TSP tmp = loadFromFile(filePath);
				if (tmp.getDimension() != 0) tsp.push_back(tmp);
				parsedFiles++;
			}
			
		}
		closedir(dir);
	}

	return tsp;
}

void FileParser::convertFile2FullMatrix(string filename)
{
	TSP tsp = loadFromFile(filename);

	int optimal;
	ofstream converted;

	converted.open(filename, ios::out);

	converted << "NAME: " << tsp.getName() << "\n";
	converted << "TYPE: " << tsp.getType() << "\n";
	cout << "Optymalne rozwiązanie " << tsp.getName() << ": ";
	cin >> optimal;
	converted << "OPTIMAL: " << optimal << "\n";
	converted << "DIMENSION: " << tsp.getDimension() << "\n";
	converted << "EDGE_WEIGHT_FORMAT: FULL_MATRIX\n";

	for (int i = 0; i < tsp.getDimension(); i++)
	{
		for (int j = 0; j < tsp.getDimension(); j++)
		{
			converted << tsp[i][j] << " ";
		}
		converted << "\n";
	}
	converted << "EOF\n";
	converted.close();
}

void FileParser::convertDirectory2FullMatrix(string path)
{
	DIR *dir;
	struct dirent *ent;

	dir = opendir(path.c_str());

	vector<TSP> tsp;

	if (dir != NULL)
	{
		while ((ent = readdir(dir)) != NULL)
		{
			if (ent->d_type == DT_REG)
			{
				numberOfFiles++;
			}
		}

		rewinddir(dir);

		while ((ent = readdir(dir)) != NULL)
		{
			if (ent->d_type == DT_REG)
			{
				cout << setw(3) << parsedFiles << "/" << setw(4) << left << numberOfFiles << setw(13) << right << ent->d_name << '\r' << flush;
				string filePath = path + "/" + ent->d_name;
				convertFile2FullMatrix(filePath);
				parsedFiles++;
			}

		}
		closedir(dir);
	}
}

vector<int> FileParser::parseLowerDiag2FullMatrix(int dimension, vector<int>Edges)
{
	vector<vector<int>> matrix(dimension, vector<int>(dimension));

	auto iter(Edges.begin());

	int i = 0, j = 0, l = 1;;
	while (iter != Edges.end())
	{
		matrix[i][j] = *iter;
		++iter;
		++j;
		if(j == l)
		{
			l++;
			++i;
			j = 0;
		}
	}

	for (i = 0; i < dimension; i++)
	{
		for (j = i; j < dimension; j++)
		{
			matrix[i][j] = matrix[j][i];
		}
	}

	Edges.resize(dimension*dimension);

	for (int i = 0; i < dimension; i++)
	{
		for (int j = 0; j < dimension; j++)
		{
			Edges[i*dimension + j] = matrix[i][j];
		}
	}
	return Edges;
}

vector<int> FileParser::parseUpperRow2FullMatrix(int dimension, vector<int> Edges)
{
	vector<vector<int>> matrix(dimension, vector<int>(dimension));

	auto iter(Edges.begin());

	int i = 0, j = 0, l = dimension - 1;
	while (iter != Edges.end())
	{
		if (j == l)
		{
			matrix[i][j] = 0;
			l--;
			++i;
			j = 0;
		}

		matrix[i][j] = *iter;
		++iter;
		++j;
		
	}

	for (i = 0; i < dimension; i++)
	{
		reverse(matrix[i].begin(), matrix[i].end());
	}

	for (i = 0; i < dimension; i++)
	{
		for (j = i; j < dimension; j++)
		{
			matrix[j][i] = matrix[i][j];
		}
	}	

	Edges.resize(dimension*dimension);

	for (int i = 0; i < dimension; i++)
	{
		for (int j = 0; j < dimension; j++)
		{
			Edges[i*dimension + j] = matrix[i][j];
		}
	}
	return Edges;
}

vector<int> FileParser::parseEuc2D2FullMatrix(int dimension, vector<float> EdgesFl)
{
	vector<vector<int>> matrix(dimension, vector<int>(dimension));

	struct Point
	{
		float X;
		float Y;
	};

	vector<Point> Points(dimension);
	auto iter = EdgesFl.begin();

	for (int i = 0; i < dimension; i++)
	{
		++iter;
		Points[i].X = *iter;
		++iter;
		Points[i].Y = *iter;
		++iter;
	}

	for (int i = 0; i < dimension; i++)
	{
		for (int j = i + 1; j < dimension; j++)
		{
			matrix[i][j] = (int) (sqrt(pow(Points[j].X - Points[i].X, 2) + pow(Points[j].Y - Points[i].Y, 2) ) + 0.5); //to 0.5 służy do zaokrąglnia
			matrix[j][i] = matrix[i][j];
		}
	}

	vector<int>Edges(dimension*dimension);

	for (int i = 0; i < dimension; i++)
	{
		for (int j = 0; j < dimension; j++)
		{
			Edges[i*dimension + j] = matrix[i][j];
		}
	}
	return Edges;
}

vector<int> FileParser::parseGeo2FullMatrix(int dimension, vector<float> EdgesFl)
{
	vector<vector<int>> matrix(dimension, vector<int>(dimension));

	struct Point
	{
		float lat;
		float lon;
	};

	vector<Point> Points(dimension);
	auto iter = EdgesFl.begin();

	for (int i = 0; i < dimension; i++)
	{
		++iter;
		Points[i].lat = *iter;
		++iter;
		Points[i].lon = *iter;
		++iter;
	}

	float Radius = 6371; // km

	for (int i = 0; i < dimension; i++)
	{
		for (int j = i + 1; j < dimension; j++)
		{
			float dLat = (Points[j].lat - Points[i].lat) * M_PI / 180.0;
			float dLon = (Points[j].lon - Points[i].lon) * M_PI / 180.0;
			float lat1 = Points[i].lat * M_PI / 180.0;
			float lat2 = Points[j].lat * M_PI / 180.0;

			auto a = pow(sin(dLat / 2.0), 2) + pow(sin(dLon / 2.0), 2) * cos(lat1) * cos(lat2);

			auto c = 2.0 * atan2(sqrt(a), sqrt(1.0 - a));

			matrix[i][j] = (int) (Radius * c + 0.5);
			matrix[j][i] = matrix[i][j];
		}
	}

	vector<int>Edges(dimension*dimension);

	for (int i = 0; i < dimension; i++)
	{
		for (int j = 0; j < dimension; j++)
		{
			Edges[i*dimension + j] = matrix[i][j];
		}
	}
	return Edges;
}

vector<int> FileParser::normalize(int dimension, vector<int> Edges)
{
	for (int i = 0; i < Edges.size(); i += dimension + 1) Edges[i] = 0;

	return Edges;
}
