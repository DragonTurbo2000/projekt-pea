#pragma once
#include <vector>
#include <list>
#include "Ant.h"
#include "TSP.h"

using namespace std;

class AntAlgorithm 
{

private:
	vector<vector<double>> Phero;
	vector<int> bestSolution;
	vector<Ant> ants;
	vector<double>prob;

	const double evapFactor = 0.1;		//Wsp�czynnik odparowywania feromonu ze �cie�ki
	const double alphaFactor = 0.1;		//Wsp�czynnik wp�ywu pozostawionego feromonu na pr wyboru �cie�ki
	const double betaFactor = 0.1;		//Wsp�czynnik wp�ywu odleg�o�ci mi�dzy miastami na pr wyboru �cie�ki

	TSP tsp;
	int dimension;
	int colonySize;
	const double pr = 0.01;
	int totalLength;

	//Dzia�ania na mr�wkach
	void initAnts();
	void runAnt(int antIndex);
	void runAllAnts();
	void calculateProb(int antIndex);

	//Dzia�ania na poziomie feromonu
	void initializePhero();
	void fadePhero();
	void placePhero(vector<int> tour);

	int drawCity(int antIndex);
	

public:
	AntAlgorithm();
	~AntAlgorithm();

	TSP startAlgorithm(TSP tsp, int colonySize, int numberOfIterations);

};

