#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <cmath>
#include <random>

using namespace std;

class TSP
{
private:
	string name = "TSP";
	string type;
	int dimension = 0;
	int optimal;
	vector<vector<int>> adjacencyMatrix;
	vector<int> solution;
	int totalDistance = 0;

public:
	TSP();
	//TSP(int dimension);
	TSP(int dimension, vector<int> Edges);
	TSP(string name, string type, int optimal, int dimension, vector<int> Edges);
	~TSP();

	string getName() const { return name; };
	string getType() const { return type; };
	int getDimension() const { return dimension; };
	int getOptimal() const { return optimal; };
	int getTotalDistance() const { return totalDistance; };
	vector<int> getSolution() const { return solution; };
	void updateSolution(vector<int> solution);

	//vector<int>& operator[](int index) { return adjacencyMatrix[index]; };
	vector<int> operator[](int index) const { return adjacencyMatrix[index]; };
	int citiesDistance(int cityA, int cityB) const { return adjacencyMatrix[cityA][cityB]; }
	string getPath();
};

string to_string(TSP tsp);
