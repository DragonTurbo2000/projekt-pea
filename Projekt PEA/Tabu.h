#pragma once
#include "Algorithm.h"
#include "TabuList.h"

class Tabu : public Algorithm
{	

	vector<int> getBestNeighbour(vector<int> order);
	vector<int> bestOrder;
	TabuList *tabuList;

	void randomSwap(vector<int> &order);
	int bestChange[2];

public:
	TSP tabu(TSP Tsp, int maxTabuSize);
};

